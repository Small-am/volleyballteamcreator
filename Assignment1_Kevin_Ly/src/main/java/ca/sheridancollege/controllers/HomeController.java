package ca.sheridancollege.controllers;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ca.sheridancollege.beans.Player;
import ca.sheridancollege.beans.Team;
import ca.sheridancollege.repositories.*;

@Controller
public class HomeController {

	@Autowired
	private playerRepository playRepo;
	
	@Autowired
	private teamRepository teamRepo;	
	
	
	
	@GetMapping("/")
	public String home(Model model)
	{
		model.addAttribute("player", new Player());
		return "home.html";
	}
	
	//Add players -----------------------------------
	@GetMapping("/addPlayer")
	public String addPlayer(@ModelAttribute Player player, Model model)
	{
		playRepo.save(player);
		model.addAttribute("players", playRepo.findAll());
		model.addAttribute("player", new Player());
		return "addPlayer.html";
	}
	
	//Edit player(s) --------------------------
	@GetMapping("/edit/{id}")
	public String editPlayer(@PathVariable int id, Model model)
	{
		Optional<Player> player = playRepo.findById(id);
		if (player.isPresent())
		{
			Player selectedPlayer = player.get();
			model.addAttribute("player", selectedPlayer);
			return "editPlayer.html";
		} else
		{
			return "redirect:/addPlayer.html";
		}
	}
	
	@PostMapping("/editPlayer")
	public String modifyPlayer(@ModelAttribute Player player)
	{
		playRepo.save(player);
		return "redirect:/addPlayer";
	}
	
	//Delete player(s) ---------------------------------------------
	@GetMapping("/delete/{id}")
	public String deletePlayer(@PathVariable int id, Model model)
	{
		playRepo.deleteById(id);
		model.addAttribute("player", new Player());
		model.addAttribute("players", playRepo.findAll());
		return "addPlayer.html";
	}
	
	//Search Player(s) -----------------------------------------------
	@GetMapping("/search")
	public String search(Model model)
	{
		model.addAttribute("players", new ArrayList<Player>());
		return "search.html";
	}
	@GetMapping("/searchName")
	public String searchName(@RequestParam String name, Model model)
	{
		model.addAttribute("players", playRepo.searchByName(name));
		return "search.html";
	}
	@GetMapping("/searchAge")
	public String searchAge(@RequestParam int age, Model model)
	{
		model.addAttribute("players", playRepo.findByAge(age));
		return "search.html";
	}
	@GetMapping("/searchGender")
	public String searchGender(@RequestParam String gender, Model model)
	{
		model.addAttribute("players", playRepo.findByGender(gender));
		return "search.html";
	}
	
	//Assign players to team ---------------------------------------
	@GetMapping("/assignPlayerToTeam")
	public String loadAssignPlayerToTeam(Model model)
	{
		model.addAttribute("players", playRepo.findAll());
		model.addAttribute("teams", teamRepo.findAll());
		return "assignPlayerToTeam.html";
	}
	
	@PostMapping("/assignPlayerToTeam")
	public String assignPlayer(@RequestParam String player, @RequestParam Integer team)
	{
		Player play = playRepo.findByName(player).get();
		Team te = teamRepo.findById(team).get();
		
		play.getTeam().add(te);
		te.getPlayers().add(play);
		
		teamRepo.save(te);
		
		return "redirect:/assignPlayerToTeam";
	}
	
	//Display Teams ----------------------------------------
	@GetMapping("/viewTeams")
	public String goViewTeams(Model model)
	{
		model.addAttribute("teams", teamRepo.findAll());
		return "viewTeams.html";
	}
	@PostMapping("/viewTeams")
	public String viewTeams(Model model, @RequestParam Integer team)
	{	
		Team te = teamRepo.findById(team).get();
		
		model.addAttribute("team", te);
		model.addAttribute("teams", teamRepo.findAll());
		return "viewTeams.html";
	}
	
	
	
	
	
	
}
