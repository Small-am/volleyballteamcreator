package ca.sheridancollege.repositories;

import org.springframework.data.repository.CrudRepository;

import ca.sheridancollege.beans.Team;

public interface teamRepository extends CrudRepository<Team, Integer>{
}
