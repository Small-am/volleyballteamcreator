package ca.sheridancollege.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import ca.sheridancollege.beans.Player;

public interface playerRepository extends CrudRepository<Player, Integer>{

	public Optional<Player> findByName(String name);
	
	public List<Player> searchByName(String name);
	public List<Player> findByAge(int age);
	public List<Player> findByGender(String gender);
}
